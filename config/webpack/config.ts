
import * as webpack from 'webpack'

import * as path from 'path'
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin'
import * as MiniCssExtractPlugin from 'mini-css-extract-plugin'
import * as UglifyJsPlugin from 'uglifyjs-webpack-plugin'
import * as OptimizeCssAssetsPlugin from 'optimize-css-assets-webpack-plugin'

import { buildConstants } from './constants'
import { buildPlugins } from './plugins'

import * as ColorFunctions from '../../src/color-functions'

function createMainConfig ({ prod = false } = {}): webpack.Configuration & { devServer: { [k: string]: any } } {
  const dev = !prod
  const constants = buildConstants(dev)

  return {
    bail: false,
    mode: dev ? 'development' : 'production',
    devtool: dev ? 'eval-source-map' : undefined,
    entry: [
      require.resolve('babel-polyfill'),
      require.resolve('../polyfills'),
      constants.entrypoint,
    ],
    devServer: {
      port: process.env.PORT ? parseInt(process.env.PORT, 10) : 3000,
      contentBase: [
        constants.staticDir,
      ],
      compress: true,
      clientLogLevel: 'none',
      watchContentBase: true,
      hot: true,
      historyApiFallback: true,
      // TODO: remove this once this issue is resolved:
      // https://github.com/webpack/webpack-dev-server/issues/1604
      disableHostCheck: true,
    },
    output: {
      path: constants.outputDir,
      pathinfo: dev,
      filename: 'static/js/[name].[hash:8].js',
      chunkFilename: 'static/js/[name].[chunkhash:8].chunk.js',
      // Point sourcemap entries to original disk location (format as URL on Windows)
      devtoolModuleFilenameTemplate: dev
        ? (info: any) =>
            path
              .resolve(info.absoluteResourcePath)
              .replace(/\\/g, '/')
        : (info: any) =>
            path
              .relative(constants.srcDir, info.absoluteResourcePath)
              .replace(/\\/g, '/'),
    },
    resolve: {
      extensions: [
        '.ts',
        '.tsx',
        '.js',
        '.jsx',
        '.json',
      ],
      plugins: [
        new TsconfigPathsPlugin({ configFile: constants.tsconfig }),
      ],
    },
    module: {
      strictExportPresence: true,
      rules: [
        {
          enforce: 'pre',
          test: /\.pcss$/,
          exclude: /node_modules/,
          loader: 'typed-css-modules-loader?noEmit',
        },
        {
          oneOf: [
            {
              test: /\.(js|jsx|ts|tsx)$/,
              exclude: /node_modules/,
              loader: 'babel-loader',
              options: {
                compact: true,
                cacheDirectory: true,
                babelrc: false,
                presets: [
                  [
                    '@babel/preset-env',
                    { targets: { browsers: 'last 2 versions' } },
                  ],
                  '@babel/preset-typescript',
                  '@babel/preset-react',
                ],
                plugins: [
                  ['@babel/plugin-proposal-decorators', { legacy: true }],
                  ['@babel/plugin-proposal-class-properties', { loose: true }],
                  dev ? 'react-hot-loader/babel' : undefined,
                ].filter(p => p),
              },
            },
            {
              test: /\.(bmp|gif|jpe?g|png|woff|woff2|eot|ttf|svg|ico)$/,
              loader: 'url-loader',
              options: {
                limit: 10000,
                name: 'static/media/[name].[hash:8].[ext]',
              },
            },
            {
              test: /\.p?css$/,
              use: [
                dev ? 'style-loader' : MiniCssExtractPlugin.loader,
                {
                  loader: 'css-loader',
                  options: {
                    modules: true,
                    localIdentName: dev ? '[name]_[local]--[hash:base64:4]' : '[hash:base64:8]',
                    importLoaders: 1,
                  },
                },
                {
                  loader: 'postcss-loader',
                  options: {
                    ident: 'postcss',
                    plugins: () => [
                      require('postcss-import'),
                      require('postcss-mixins'),
                      require('postcss-each'),
                      require('postcss-simple-vars'),
                      require('postcss-functions')({
                        functions: ColorFunctions,
                      }),
                      require('postcss-color-function'),
                      require('postcss-preset-env'),
                      require('postcss-nested'),
                      require('autoprefixer')({
                        browsers: [
                          '>1%',
                          'last 4 versions',
                          'Firefox ESR',
                          'not ie < 11',
                        ],
                        flexbox: 'no-2009',
                      }),
                    ],
                  },
                },
              ],
            },
            {
              exclude: /\.(js|jsx|ts|tsx|html|json|css|pcss)$/,
              loader: require.resolve('file-loader'),
              options: {
                name: 'static/media/[name].[hash:8].[ext]',
              },
            },
          ],
        },
      ],
    },
    plugins: buildPlugins(dev, constants),
    node: {
      dgram: 'empty',
      fs: 'empty',
      net: 'empty',
      tls: 'empty',
      child_process: 'empty',
    },
    performance: {
      hints: dev ? false : 'warning',
    },
    optimization: !dev ? {
      minimizer: [
        new UglifyJsPlugin({
          uglifyOptions: {
            output: {
              beautify: false,
              indent_level: 2,
            },
          },
          parallel: true,
          cache: 'uglify-cache',
          sourceMap: false,
        }),
        new OptimizeCssAssetsPlugin({}),
      ],
    } : {
      namedModules: true,
    },
  }
}

export default createMainConfig
