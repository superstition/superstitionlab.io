
import * as React from 'react'

import { $lowerName } from './$UpperName.pcss'

export class $UpperName extends React.Component {
  render () {
    return (
      <div className={$lowerName}>
        $UpperNameComponent Content
      </div>
    )
  }
}
