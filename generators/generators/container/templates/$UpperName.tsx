
import * as React from 'react'
import { observer, inject } from 'mobx-react'
import { AuthProp } from 'state/authStore'

import { $lowerName } from './$UpperName.pcss'

interface Props extends AuthProp {
}

@inject('auth')
@observer
class $UpperName extends React.Component<Props> {

  public render () {
    const { auth } = this.props

    return (
      <div className={$lowerName}>
        {
          auth!.jwt
          ? <div>You're logged in</div>
          : <div>You're not logged in</div>
        }
      </div>
    )
  }
}

export default $UpperName
