
import * as React from 'react'

import { $lowerName } from './$UpperName.pcss'

export class $UpperName extends React.PureComponent {
  render () {
    return (
      <div className={$lowerName}>
        $UpperNameComponent Content
      </div>
    )
  }
}
