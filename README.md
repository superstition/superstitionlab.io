# Superstition
[![pipeline status](https://gitlab.com/superstition/superstition.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/superstition/superstition.gitlab.io/commits/master)
[![coverage report](https://gitlab.com/superstition/superstition.gitlab.io/badges/master/coverage.svg)](https://gitlab.com/superstition/superstition.gitlab.io/commits/master)

[Check it out](https://superstition.gitlab.io)