
import * as chaiString from 'chai-string'
chai.use(chaiString)

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

function wrapIfNecessary<T> (item: T): T extends Cypress.Chainable ? T : Cypress.Chainable<T> {
  const chainable = item as any as Cypress.Chainable
  if (
    typeof chainable.should === 'function' &&
    typeof chainable.get === 'function' &&
    typeof chainable.its === 'function' &&
    typeof chainable.find === 'function' &&
    typeof chainable.filter === 'function'
  ) {
    return item as any
  } else {
    return cy.wrap(item, { log: false }) as any
  }
}

function unwrapJquery<T> (item: T | JQuery<T>): T {
  const jq = item as JQuery<any>
  if (
    typeof jq.length === 'number'
  ) {
    return jq[0]
  } else {
    return item as T
  }
}

Cypress.Commands.add(
  'shouldHaveCssVar',
  { prevSubject: 'optional' },
  shouldHaveCssVar,
)

function shouldHaveCssVar (
  subject: Cypress.Chainable<HTMLElement> | undefined,
  cssVarName: string,
  expectedValue: string,
  { caseSensitive = false, trim = true }: Cypress.ShouldHaveCssVarOptions = {},
) {
  if (trim) {
    expectedValue = expectedValue.trim()
  }

  const log = Cypress.log({
    name: 'CSS_VAR',
    $el: subject as any,
    consoleProps: () => ({
      'CSS Variable': cssVarName,
      'Expected Value': expectedValue,
    }),
  })

  subject = wrapIfNecessary(subject || cy.document({ log: false }).then(doc => doc.documentElement))

  subject.should(($el: HTMLElement | JQuery) => {
    const htmlEl = unwrapJquery($el)
    let actualValue = getComputedStyle(htmlEl).getPropertyValue(cssVarName)
    if (trim) {
      actualValue = actualValue.trim()
    }

    if (caseSensitive) {
      expect(actualValue).to.equal(expectedValue)
    } else {
      expect(actualValue).to.equalIgnoreCase(expectedValue)
    }
    log.end()
  })

  log.snapshot().end()

  return subject
}
