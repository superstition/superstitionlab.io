
import * as React from 'react'
import { observer, inject } from 'mobx-react'
import { ColorProp } from 'state/colorStore'

import ColorPicker from 'components/ColorPicker'
import Button from 'components/Button'

import { styler, open, chooser, toggler, closer } from './Styler.pcss'

interface Props extends ColorProp {
}

interface State {
  isOpen: boolean
}

@inject('color')
@observer
class Styler extends React.Component<Props, State> {
  state: State = {
    isOpen: false,
  }

  public render () {
    const { color } = this.props
    const { isOpen } = this.state

    const classes = [styler]
    if (isOpen) classes.push(open)

    return (
      <div className={classes.join(' ')}>
        <div className={closer} onClick={() => this._toggleOpen(false)} />
        <div className={chooser}>
          <ColorPicker
            id='primaryColor'
            label='Primary'
            onChange={this._setPrimary}
            value={color!.primary}
          />
          <ColorPicker
            id='secondaryColor'
            label='Secondary'
            onChange={this._setSecondary}
            value={color!.secondary}
          />
          <ColorPicker
            id='tertiaryColor'
            label='Tertiary'
            onChange={this._setTertiary}
            value={color!.tertiary}
          />
          <div>
            <Button id='resetColors' color='primary' onClick={() => this.props.color!.resetColors()}>Reset colors</Button>
          </div>
        </div>
        <Button id='stylerToggle' className={toggler} color={'secondary'} onClick={this._toggleOpen}>
          C
        </Button>
      </div>
    )
  }

  private _toggleOpen = (isOpen = !this.state.isOpen) => this.setState({ isOpen })

  private _setPrimary = (value: string) => this.props.color!.primary = value
  private _setSecondary = (value: string) => this.props.color!.secondary = value
  private _setTertiary = (value: string) => this.props.color!.tertiary = value
}

export default Styler
