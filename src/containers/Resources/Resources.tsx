
import * as React from 'react'
import { observer, inject } from 'mobx-react'
import { ResourceProp } from 'state/resourceStore'

import ResourceDisplay from 'components/ResourceDisplay'

// import { styler, open, chooser, toggler, closer } from './Styler.pcss'

interface Props extends ResourceProp {
}

@inject('resources')
@observer
class Resources extends React.Component<Props> {

  public render () {
    const { resources } = this.props

    return (
      <div>
        <ResourceDisplay
          name='Ectoplasm'
          value={resources!.ectoplasm}
          rate={resources!.ectoplasmPerSecond}
        />
      </div>
    )
  }
}

export default Resources
