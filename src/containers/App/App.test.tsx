
import * as React from 'react'
import { shallow } from 'enzyme'

import App from './App'

describe(App.name, () => {
  it('should mount without throwing an error', () => {
    expect(shallow(<App />)).toMatchSnapshot()
  })
})
