import * as React from 'react'
import MobxProvider from 'state/MobxProvider'

import Styler from 'containers/Styler'
import Resources from 'containers/Resources'
import InstallButton from 'containers/InstallButton'

import { bootstrap } from '../../logic'

import {
  app,
  header,
  title,
  subtitle,
  content,
  footer,
} from './App.pcss'

class App extends React.Component {
  componentDidMount () {
    bootstrap()
  }

  public render () {
    return (
        <MobxProvider>
          <div className={app}>
            <div className={header}>
              <div className={title}>Superstition</div>
              <div className={subtitle}>An idle game about the supernatural</div>
            </div>
            <div className={content}>
              <Resources />
            </div>
            <div className={footer}>
              <div>
              Kevin Groat © 2019 • <a href='https://gitlab.com/superstition/superstition.gitlab.io/blob/master/LICENSE'>ISC License</a>
              </div>
              <div>
                <a href='https://gitlab.com/superstition/superstition.gitlab.io'>
                  View the source on GitLab
                </a>
              </div>
              <InstallButton />
            </div>
            <Styler />
          </div>
        </MobxProvider>
    )
  }
}

export default App
