import * as React from 'react'

import Button from 'components/Button'

import { canInstall, install } from '../../registerServiceWorker'
import { installButton } from './InstallButton.pcss'

const InstallButton = () => (
  canInstall
  ? <Button className={installButton} onClick={install}>I</Button>
  : <div style={{ display: 'none' }}/>
)

export default InstallButton
