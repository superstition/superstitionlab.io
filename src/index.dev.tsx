
import './global-css/index.pcss'

import * as React from 'react'
import * as ReactDOM from 'react-dom'
import DevTools from 'mobx-react-devtools'
import 'roboto-fontface/css/roboto/roboto-fontface.css'

import App from 'containers/App/index.dev'

ReactDOM.render(
  <div id='dev-wrapper' style={{ height: '100%' }}>
    <App />
    {process.env.NODE_ENV === 'development' && <DevTools />}
  </div>,
  document.getElementById('root') as HTMLElement,
)
