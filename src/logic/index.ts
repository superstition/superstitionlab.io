
import { action } from 'mobx'

import { resources } from 'state/resourceStore'
import { time } from 'state/timeStore'

let msSinceStart = performance.now()

const tickAction = action(() => {
  const newMs = performance.now()
  const diffSeconds = (newMs - msSinceStart) / 1000
  msSinceStart = newMs

  const newTime = Date.now()
  const realSecondsDiff = (newTime - time.currentTime) / 1000
  time.currentTime = newTime

  // if performance.now() is off by 10 seconds, it likely means the game was sleeping a while
  // in that case, we should use the time given by Date.now(), as it is more likely to be accurate
  if (realSecondsDiff - diffSeconds > 10) {
    resources.tick(realSecondsDiff)
  } else {
    resources.tick(diffSeconds)
  }
})

function tick () {
  tickAction()
  requestAnimationFrame(tick)
}

export function bootstrap () {
  requestAnimationFrame(tick)
}
