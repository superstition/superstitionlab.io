
import { observable } from 'mobx'
import { NumberNotation } from 'helpers/notation'

import autoSave from '../autoSave'

export interface SettingsProp {
  settings?: SettingsStore
}

class SettingsStore {
  @observable numberNotation: NumberNotation = 'traditional'
}

// export const color = new ColorStore()
export const settings = autoSave('settings')(new SettingsStore())
