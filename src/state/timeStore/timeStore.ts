
import { observable } from 'mobx'

import autoSave from '../autoSave'

export interface TimeProp {
  time?: TimeStore
}

class TimeStore {
  @observable currentTime: number = Date.now()
}

export const time = autoSave('time')(new TimeStore())
