
import * as React from 'react'
import { Provider } from 'mobx-react'

import { ColorProp, color } from './colorStore'
import { ResourceProp, resources } from './resourceStore'
import { SettingsProp, settings } from './settingsStore'
import { TimeProp, time } from './timeStore'

interface Props {
  children?: React.ReactNode,
}

type ProviderProps =
  & Required<ColorProp>
  & Required<SettingsProp>
  & Required<ResourceProp>
  & Required<TimeProp>

const stores: ProviderProps = {
  color,
  settings,
  resources,
  time,
}

const MobxProvider = ({ children }: Props) => (
  <Provider {...stores}>
    {children}
  </Provider>
)

export default MobxProvider
