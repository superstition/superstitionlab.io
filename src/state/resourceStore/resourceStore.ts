
import { observable, action } from 'mobx'

import autoSave from '../autoSave'

export interface ResourceProp {
  resources?: ResourceStore
}

class ResourceStore {
  @observable ectoplasm = 0
  @observable ectoplasmPerSecond: number | undefined = 1

  @action
  tick (seconds: number) {
    if (this.ectoplasmPerSecond !== undefined) {
      this.ectoplasm = this.ectoplasm + this.ectoplasmPerSecond * seconds
    }
  }
}

export const resources = autoSave('resources')(new ResourceStore())
