
import { settings } from 'state/settingsStore'

import { printDigits } from './numbers'

const kiloMap = [
  'Ki', 'Mi', 'Bi', 'Tr', 'Qa',
  'Qi', 'Se', 'Sx', 'Oc', 'No',
]

const unMap = [
  '', 'Un', 'Du', 'Tr', 'Qa',
  'Qi', 'Se', 'Sx', 'Oc', 'No',
]

const decillionMap = [
  'De', 'Vi', 'Tr', 'Qa', 'Qi',
  'Se', 'Sx', 'Oc', 'No',
]

export function encodeValueTraditional (value: number): string {
  if (value < 1000) {
    return printDigits(value, 1)
  } else {
    const zeroes = Math.floor(Math.log10(value))
    const offset = Math.floor(zeroes / 3) - 1
    const digits = 2 - zeroes + (offset * 3) + 3
    const partialValue = value / Math.pow(1000, offset + 1)
    const partialValueStr = printDigits(partialValue, digits)

    if (offset < kiloMap.length) {
      return `${partialValueStr} ${kiloMap[offset]}`
    } else {
      const decaOffset = Math.floor(offset / 10) - 1
      return `${partialValueStr} ${unMap[offset % 10]}${decillionMap[decaOffset]}`
    }
  }
}

export function encodeValueScientific (value: number): string {
  if (value < 1000) {
    return printDigits(value, 1)
  } else {
    const zeroes = Math.floor(Math.log10(value))
    const partialValue = value / Math.pow(10, zeroes)
    const partialValueStr = printDigits(partialValue, 2)

    return `${partialValueStr} e${zeroes}`
  }
}

export type NumberNotation = 'traditional' | 'scientific'

export function encodeValue (value: number, notation: NumberNotation = settings.numberNotation) {
  if (notation === 'traditional') {
    return encodeValueTraditional(value)
  } else if (notation === 'scientific') {
    return encodeValueScientific(value)
  } else {
    console.error(`Unrecognized encoding ${notation}; falling back to scientific`)
    return encodeValueScientific(value)
  }
}
