import * as React from 'react'

import { encodeValue, NumberNotation } from 'helpers/notation'

import { resourceDisplay, resourceName, content, resourceValue, resourceRate } from './ResourceDisplay.pcss'

interface Props {
  name: string
  value: number
  rate?: number
  notation?: NumberNotation
}

function ResourceDisplay ({ name, value, rate, notation }: Props) {
  return (
    <div className={resourceDisplay}>
      <div className={resourceName}>{name}</div>
      <div className={content}>
        <div className={resourceValue}>{encodeValue(value, notation)}</div>
        {
          rate !== undefined &&
          <div className={resourceRate}>
            +{encodeValue(rate, notation)} / s
          </div>
        }
      </div>
    </div>
  )
}

export default ResourceDisplay
