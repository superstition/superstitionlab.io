import * as React from 'react'

import * as styles from './Button.pcss'

interface Props {
  id?: string
  children: React.ReactNode
  className?: string
  color?: typeof DEFAULT | 'primary' | 'secondary' | 'tertiary'
  size?: typeof DEFAULT | 'small' | 'large' | 'x-large'
  onClick: () => void
}

const DEFAULT = 'default'

class Button extends React.Component<Props> {
  public render () {
    const { id, children, className, color = DEFAULT, size = DEFAULT } = this.props

    const classes = [styles.button]
    if (className) {
      classes.push(className)
    }

    if (color !== DEFAULT) {
      classes.push(styles[`${color}-color`])
    }

    if (size !== DEFAULT) {
      classes.push(styles[`${size}-size`])
    }

    return (
      <button id={id} className={classes.join(' ')} type='button' onClick={this._handleClick}>
        {children}
      </button>
    )
  }

  private _handleClick = (ev: React.MouseEvent<HTMLButtonElement>) => {
    ev.preventDefault()
    this.props.onClick()
  }
}

export default Button
