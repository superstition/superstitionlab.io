import * as React from 'react'

import { colorPicker, colorTextInput } from './ColorPicker.pcss'

interface Props {
  id: string
  label: string
  value?: string
  onChange: (newValue: string) => void
}

interface State {
  typedColor: string
}

const colorRgx = /^#[0-9A-F]{6}$/
const partialColorRgx = /^#?[0-9A-F]{0,6}$/

class ColorPicker extends React.Component<Props, State> {
  state: State = {
    typedColor: this.props.value!,
  }

  textInputRef = React.createRef<HTMLInputElement>()

  componentDidUpdate (prevProps: Props) {
    if (prevProps.value !== this.props.value && this.props.value !== undefined) {
      this.setState({ typedColor: this.props.value })
    }
  }

  public render () {
    const { id, label, value = '#000000' } = this.props

    return (
      <div className={colorPicker}>
        <label htmlFor={id}>
          {label}:&nbsp;&nbsp;
          <input
            id={id}
            type='color'
            value={value}
            onChange={this._handleChange}
          />
        </label>
        <input
            id={`${id}-text`}
            type='text'
            ref={this.textInputRef}
            className={colorTextInput}
            value={this.state.typedColor}
            onChange={this._handleTextChange}
          />
      </div>
    )
  }

  private _handleChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
    this.props.onChange(ev.currentTarget.value.toUpperCase())
  }

  private _handleTextChange = (ev: React.ChangeEvent<HTMLInputElement>) => {
    let selectionStart = ev.currentTarget.selectionStart!
    let typedColor = ev.currentTarget.value.toUpperCase()
    if (!partialColorRgx.test(typedColor)) {
      ev.preventDefault()
      selectionStart--
      this.setState({}, () => {
        if (selectionStart && this.textInputRef.current) {
          this.textInputRef.current.selectionStart = selectionStart
          this.textInputRef.current.selectionEnd = selectionStart
        }
      })
      return
    }

    if (typedColor[0] !== '#') {
      typedColor = `#${typedColor}`
      selectionStart++
    }

    this.setState({ typedColor }, () => {
      if (selectionStart && this.textInputRef.current) {
        this.textInputRef.current.selectionStart = selectionStart
        this.textInputRef.current.selectionEnd = selectionStart
      }
    })

    if (colorRgx.test(typedColor)) {
      this.props.onChange(typedColor)
    }
  }
}

export default ColorPicker
